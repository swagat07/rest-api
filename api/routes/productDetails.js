const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const mysql = require('mysql');

var mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database:'ecommerce',
    insecureAuth : true
});

mysqlConnection.connect((err) =>{
    if(!err)
    console.log('database connection success');
    else
    console.log('db conn fail');
});

router.use(bodyParser.urlencoded({extended: false}));
router.use(bodyParser.json());

//get details of all the products present in a list
router.get('/details',(req,res) =>{
   mysqlConnection.query('SELECT * FROM productDetails',(err,rows) =>{
       if(!err)
       res.send(JSON.stringify({"status":200,"error":null,"response":rows}));
       else
       console.log(err);
       res.send(JSON.stringify({"status":400,"error":err,"response":null}));
   });
});

// get One product list
router.get('/:product_id',(req,res) =>{
    mysqlConnection.query('SELECT * FROM productDetails WHERE product_id = ?',[req.params.product_id],(err,rows,fields) =>{
        if(!err)
        res.send(JSON.stringify({"status":200,"error":null,"response":rows}));
        else
        console.log(JSON.stringify({"status":400,"error":err,"response":null}));
    });
 });

 // Insert product details
 router.post('/details',(req, res) => {
     product_id: req.body.product_id;name: req.body.name;price:req.body.price;description:req.body.description;
    let sql = "INSERT INTO productDetails SET ?";
     mysqlConnection.query(sql, req.body,(err, results) => {
      if(err) throw err;
      res.send(results);
    });
  });

  // Get all product list
  router.get('/', (req,res) =>{
    mysqlConnection.query('SELECT product_id,name FROM productDetails',(err,rows,fields) =>{
        if(!err)
        res.send(JSON.stringify({"status":200,"error":null,"response":rows}));
        else
        console.log(JSON.stringify({"status":400,"error":err,"response":null}));
    });
});
 // Get user data
 router.get('/userdetail/:userId',(req,res) =>{
     mysqlConnection.query('SELECT * FROM userInfo WHERE userId = ?',[req.params.userId],(err,rows,fields) =>{
         if(!err)
         res.send(JSON.stringify({"status":200,"error":null,"response":rows}));
         else
         console.log(JSON.stringify({"status":400,"error":err,"response":null}));
     });
  });
 // post user data
  router.post('/userdetails',(req, res) => {
     userId: req.body.userId;userName: req.body.userName;userMail: req.body.userMail;phoneNo: req.body.phoneNo;address: req.body.address;
     let sql = "INSERT INTO userInfo SET ?";
      mysqlConnection.query(sql, req.body,(err, results) => {
       if(err) throw err;
       res.send(results);
     });
   });
// get the checkout of a single user
 router.get('/addtocart/checkout/:userId',(req,res) =>{
     mysqlConnection.query('select count(cart.userId)as total_no_products,userinfo.userName,sum(cart.no_of_product*productDetails.price)as total from cart join productDetails on (cart.product_id=productDetails.product_id) join userinfo on(cart.userId=userinfo.userId) where (cart.userId=?) group by cart.userId',[req.params.userId],(err,rows,fields) =>{
         if(!err)
         res.send(JSON.stringify({"status":200,"error":null,"response":rows}));
         else
         console.log(JSON.stringify({"status":400,"error":err,"response":null}));
     });
 });
// get addtocart details of a single user
     router.get('/addtocart/:userId',(req,res) =>{
         mysqlConnection.query('SELECT userInfo.userName,cart.product_id,productdetails.name as productName,cart.no_of_product,cart.no_of_product*productdetails.price as total FROM cart JOIN userInfo ON (cart.userId=userInfo.userId) join productDetails on (cart.product_id=productDetails.product_id) where cart.userId = ?',[req.params.userId],(err,rows,fields) =>{
             if(!err)
             res.send(JSON.stringify({"status":200,"error":null,"response":rows}));
            else
             console.log(JSON.stringify({"status":400,"error":err,"response":null}));
         });
      });
 // add a product to addtocart
 router.post('/addtocart',(req, res) => {
    let data = {orderid: req.body.orderid,product_id: req.body.product_id,userId:req.body.userId,no_of_product:req.body.no_of_product};
    let sql = "INSERT INTO cart SET ?";
     mysqlConnection.query(sql, data,(err, results) => {
      if(err){
          console.log(err);
          res.send('This product is not available here');
      }
      else
      res.send('This product is added to your cart');
    });
  });

 router.delete('/addtocart/:userId/:product_id',(req,res) =>{
     mysqlConnection.query('DELETE FROM cart WHERE product_id = ?',[req.params.product_id],(err,rows,fields) =>{
         if(!err){
             console.log('delete running');
             res.send('deleted the data!!!');
         }  
         else
         console.log(JSON.stringify({"status":400,"error":err,"response":null}));
     });
 });

 
module.exports = router;