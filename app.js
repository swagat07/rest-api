const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const productDetails = require('./api/routes/productDetails');

app.use(bodyParser.json());      
app.use(bodyParser.urlencoded({extended: true}));

app.use('/product',productDetails);

app.use((req,res)=>{
    res.status(200).json({
        msg : 'app is running'
    });
});

module.exports=app;